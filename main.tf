# google_client_config is configured here and used in providers.tf.
data "google_client_config" "default" {}

# Import the tf-gke-gitlab-bootstrap module which configures the GitLab installation, along with
# tools to aid in Gitaly on Kubernetes development work.
module "gitalydev" {
  source  = "gitlab.com/gl-gitaly/tf-gke-gitlab-bootstrap/gitaly"
  version = "2.8.0"

  # Pipeline
  timeout = 2400 # 40 minutes; typical deployment should take ~20 minutes

  # Deployment
  random_env_suffix = trimprefix(var.env_prefix, "env-")
  gcp_region        = var.gcp_region
  gcp_project       = var.gcp_project
  dns_zone_name     = lower(var.gcp_dns_zone_name)

  # Cluster
  cluster_name           = "gitaly"
  gke_kubernetes_version = "1.28"

  # Tools
  enable_kubeshark = true

  # GitLab Helm Chart - https://gitlab.com/gitlab-org/charts/gitlab
  helm_values_gitlab = yamlencode({
    gitlab = {
      toolbox = {
        backups = {
          cron = {
            enabled                    = false
            schedule                   = "*/5 * * * *" # every 5 mins
            successfulJobsHistoryLimit = 3
            suspend                    = false
            backoffLimit               = 6
            safeToEvict                = false
          }
        }
      }
      gitaly = {
        image = {
          repository = "registry.gitlab.com/gitlab-org/build/cng/gitaly"
          pullPolicy = "IfNotPresent"
          tag        = "v16.10.0"
        },
        cgroups = {
          enabled = true
        },
        securityContext = {
          runAsUser  = 1000
          fsGroup    = 1000
          runAsGroup = 1000
        },
        resources = {
          requests = {
            cpu    = "2000m"
            memory = "4Gi"
          },
          limits = {
            cpu    = "2000m"
            memory = "4Gi"
          }
        },
        packObjectsCache = {
          enabled = true,
          max_age = "5m"
        }
      }
    }
  })
}
