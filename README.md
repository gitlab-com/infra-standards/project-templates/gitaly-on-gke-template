# Gitaly on GKE - Template

This repository is a [HackyStack/Sandbox Cloud](https://handbook.gitlab.com/handbook/infrastructure-standards/realms/sandbox/)
template for a Terraform environment that deploys a full GitLab installation onto GKE. It provisions infrastructure on GCP and
uses the [GitLab Helm Chart](https://gitlab.com/gitlab-org/charts/gitlab) to deploy GitLab and Gitaly.

The deployment gives you:

1. A publicly-accessible, full GitLab installation on a GKE cluster in your GCP project.
1. A means of monitoring objects in the cluster using `kubectl` and the [k9s](https://github.com/derailed/k9s) tools.
1. The ability to SSH into pods, for example so you can execute the Rails console.
1. The ability to port-forward the `/debug/pprof` endpoint configured in Gitaly to `localhost`, so you can use `go tool
   pprof` to collect CPU and memory metrics. This endpoint is configured courtesy of
   [Prometheus](https://gitlab.com/gitlab-org/gitaly/-/blob/e92f4647cc18e13edcaa04b2f685085b42feb6f7/internal/cli/gitaly/serve.go#L499).

## Prerequisites

Follow these steps if this is your first time setting up a GCP project via HackyStack.

### 1. Create a HackyStack/GitLab Sandbox account

Follow the guide on the [Sandbox Cloud
Realm](https://handbook.gitlab.com/handbook/infrastructure-standards/realms/sandbox/#individual-aws-account-or-gcp-project)
handbook page to create an **Individual GCP Project**. HackyStack will create a GCP project for you in the form
of <name-random_string>, e.g. `jliu-78084cdd`.

### 2. Configure `gcloud`

Ensure you have the `gcloud` CLI installed and configured. You should be able to run `gcloud config get-value project`
and see the ID of your individual GCP project as the output.

### 3. Create a HackyStack Terraform environment

1. Navigate to the [Terraform Environments](https://gitlabsandbox.cloud/environments) page on HackyStack.
1. Click **Create Terraform Environment**.
1. Under `Cloud Account`, select your individual GCP project.
1. Under `Environment Template`, select the template that begins with `gitaly-on-gke-template`.
1. Enter any value for `Environment Name`.
1. Click **Create Environment**.
1. Once the environment has been created, navigate back to the [Terraform Environments](https://gitlabsandbox.cloud/environments)
   page.
1. Click **View GitOps Credentials** and note down your GitLab username and password.
1. Click the book icon next to the environment you just created to navigate to its associated GitLab repository. The
   repository will be a mirror of what you're seeing now.
1. Continue down this README in your new repository!

## Deployment

If you're at this stage, you should be reading this file in a copy of this repository that HackyStack made in your
namespace of the [gitops.gitlabsandbox.com](https://gitops.gitlabsandbox.cloud/hackystack-environments) GitLab
instance.

To deploy your GitLab installation:

1. Kick off a new pipeline on the default branch.
1. Once the `Validate and Plan` job completes successfully, manually execute the `Everything` job.

The entire deployment should take no more than 30 minutes. The default timeout for deployment is set to 40 minutes.

## Interacting with the cluster

### 1. Install `kubectl`

Install and configure `kubectl` by following the [Install kubectl and configure cluster access](https://cloud.google.com/kubernetes-engine/docs/how-to/cluster-access-for-kubectl)
guide.

### 2. Get instance details

View the output of the `Everything` job. You'll see three values at the bottom of the logs:

```
gitlab_init_pass_cmd = "kubectl get secret gitlab-gitlab-initial-root-password -n gitlab -ojsonpath='{.data.password}' | base64 --decode ; echo"
gke_public_dns = "https://<something>.gcp.gitlabsandbox.net"
kubeconfig_update = "gcloud container clusters get-credentials <cluster_name> --region <region> --project <project_id>"
```

1. Configure `kubectl` using `gcloud` by running the `kubeconfig_update` command.
1. Get your root password using the `gitlab_init_pass_cmd` command.
1. Navigate to your new GitLab instance by clicking the `gke_public_dns` URL.

### 3. Interact with the cluster

Use `kubectl` and/or [k9s](https://github.com/derailed/k9s) to interrogate the cluster. `k9s` provides a nice
TUI with useful shortcuts for SSHing and port-forwarding pods.

## Scaling the deployment

By default, an [n2-standard-4](https://cloud.google.com/compute/docs/general-purpose-machines#n2_series) machine type
is used for the Kubernetes node. This is declared in the [variables.tf](https://gitlab.com/gl-gitaly/tf-gke-gitlab-bootstrap/-/blob/main/variables.tf)
file of the imported `tf-gke-gitlab-bootstrap` module.

If you need to scale up for testing purposes, you can make the following changes to `main.tf` within this
repository:

1. Within the `gitalydev` module, declare a `gke_node_pools` variable to override the defaults set in the
   imported `tf-gke-gitlab-bootstrap` module. Here, we declare an additional node in the pool running on the
   more expensive `c3d-standard-60` machine type:

```hcl
gke_node_pools = [
    {
      name               = "gke-01"
      machine_type       = "n2d-standard-4"
      spot               = true
      auto_repair        = true
      auto_upgrade       = true
      autoscaling        = true
      initial_node_count = 1
      total_min_count    = 0
      total_max_count    = 30
      disk_type          = "pd-balanced"
      disk_size_gb       = 100
      image_type         = "COS_CONTAINERD"
    },    
    {
      name               = "gke-02"
      machine_type       = "c3d-standard-60" 
      spot               = true
      auto_repair        = true
      auto_upgrade       = true
      autoscaling        = true
      initial_node_count = 1
      total_min_count    = 0
      total_max_count    = 30
      disk_type          = "pd-balanced"
      disk_size_gb       = 100
      image_type         = "COS_CONTAINERD"
    },
  ]
```

2. Within the `helm_values_gitlab` variable, update the `gitaly.resources` values to reflect the scale you need. The
   GKE autoscaler [optimises for cost](https://cloud.google.com/kubernetes-engine/docs/concepts/cluster-autoscaler#operating_criteria)
   by default, so pods will be scheduled on the cheapest nodes first. Here, we increase the CPU limit to 32 cores,
   and memory to 48GB:

```hcl
helm_values_gitlab = yamlencode({
  gitlab = {
    gitaly = {
      resources = {
        requests = {
          cpu    = "32000m"
          memory = "48Gi"
        },
        limits = {
          cpu    = "32000m"
          memory = "48Gi"
        }
      }
    }
  }
})
```

3. Push the changes, and re-run the `Everything` CI job to deploy the changes.

**Note:** ensure that the node pool contains enough hardware resources to satisfy the resource `requests` and
`limits` blocks of each container.

