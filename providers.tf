terraform {
  backend "http" {
    # see .gitlab-ci.yml jobs that use terraform-init -backend-config variables.
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 5
  }
}

provider "kubernetes" {
  host = "https://${module.gitalydev.cluster.endpoint}"

  token                  = data.google_client_config.default.access_token
  cluster_ca_certificate = base64decode(module.gitalydev.cluster.ca_certificate)
}

provider "helm" {
  kubernetes {
    host = "https://${module.gitalydev.cluster.endpoint}"

    token                  = data.google_client_config.default.access_token
    cluster_ca_certificate = base64decode(module.gitalydev.cluster.ca_certificate)
  }
}

# define the google cloud provider
provider "google" {
  project = var.gcp_project
  region  = var.gcp_region
}

# define the google cloud provider with beta features
provider "google-beta" {
  project = var.gcp_project
  region  = var.gcp_region
}
