variable "gcp_dns_zone_name" {
  type        = string
  description = "The GCP Cloud DNS zone name for this environment. This is automatically generated and set in the GitLab Project CI variables."
}

variable "gcp_project" {
  type        = string
  description = "The GCP project ID that the resources will be deployed in. This is automatically generated and set in the GitLab Project CI variables."
}

variable "gcp_region" {
  type        = string
  description = "The GCP region that the resources will be deployed in. (Ex. us-east1)"
  default     = "us-east1"
}

variable "env_prefix" {
  type        = string
  description = "A unique environment name. This is automatically generated and set in the GitLab Project CI variables."
}
