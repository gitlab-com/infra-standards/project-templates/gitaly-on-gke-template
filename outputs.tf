output "gke_public_dns" {
  description = "Public dns endpoint to access deployed GitLab instance"
  value       = module.gitalydev.gitlab_public_dns
}

output "gitlab_init_pass_cmd" {
  description = "Command to run on your machine to get initial GitLab root password"
  value       = module.gitalydev.gitlab_init_pass_cmd
}

output "kubeconfig_update" {
  description = "Command to run on your machine to connect your kubectl to the cluster"
  value       = module.gitalydev.kubeconfig_update
}

